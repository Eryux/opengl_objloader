# OpenGL OBJ Loader

## Fonctionnement

* Lancer l'application en spécifiant le fichier à charger :

```
objloader.exe <obj_file> <path>
```

* Pour naviguer dans la scène utiliser les touches ZQSD

* Pour switcher entre le mode caméra libre et caméra FPS utiliser la touche A

* Pour activer / désactiver la vue wireframe, utiliser la touche W

* Pour switcher entre le lambert et le phong, utiliser la touche P

* Vous pouvez augmenter et réduire la vitesse de déplacement de la caméra avec les touches + et -

## Auteur

Nicolas Candia, ESGI 2017.

## License

MIT 