#include "Camera.h"

using namespace Esgi;

Camera::Camera(vec3 position, vec3 up, GLfloat yaw, GLfloat pitch) : m_front(vec3(0.f, 0.f, -1.f)), m_speed(1.f), m_mouseSensitivity(.8f), m_fpsView(false)
{
	this->m_position = position;
	this->m_worldup = up;
	this->m_yaw = yaw;
	this->m_pitch = pitch;

	this->updateCameraVectors();
}

Mat4 Camera::GetViewMatrix()
{
	return Mat4::LookAt(this->m_position, this->m_position + this->m_front, this->m_up);
}

void Camera::updateCameraVectors()
{
	vec3 f;

	float ryaw = this->m_yaw * (M_PI / 180.f);
	float rpitch = this->m_pitch * (M_PI / 180.f);

	// Utilisation eq sphere
	f.x = cos(ryaw) * cos(rpitch);
	f.y = sin(rpitch);
	f.z = sin(ryaw) * cos(rpitch);

	this->m_front = f; this->m_front.Normalize();

	this->m_right = vec3::Cross(this->m_front, this->m_worldup);
	this->m_right.Normalize();

	this->m_up = vec3::Cross(this->m_right, this->m_front);
	this->m_up.Normalize();
}

void Camera::MoveCamera(int dir, GLfloat deltaTime)
{
	GLfloat velocity = this->m_speed * deltaTime;
	
	if (!this->m_fpsView) {
		if (dir == 0)
			this->m_position += this->m_front * velocity;
		if (dir == 1)
			this->m_position -= this->m_front * velocity;
	}
	else {
		vec3 npos = vec3(this->m_front.x, 0.f, this->m_front.z);

		if (dir == 0) 
			this->m_position += npos * velocity;
		if (dir == 1)
			this->m_position -= npos * velocity;
	}

	if (dir == 2)
		this->m_position += this->m_right * velocity;
	if (dir == 3)
		this->m_position -= this->m_right * velocity;
}

void Camera::TurnCamera(GLfloat x, GLfloat y, GLboolean lockPitch) {
	x *= this->m_mouseSensitivity; y *= this->m_mouseSensitivity;
	this->m_yaw -= x;
	this->m_pitch -= y;

	if (lockPitch) {
		if (this->m_pitch > 89.f)
			this->m_pitch = 89.f;
		if (this->m_pitch < -89.f)
			this->m_pitch = -89.0f;
	}

	this->updateCameraVectors();
}