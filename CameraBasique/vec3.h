#pragma once

#ifndef ESGI_VEC3_H
#define ESGI_VEC3_H

#define _USE_MATH_DEFINES
#include <cmath>

namespace Esgi
{
	struct vec3
	{
		vec3() {}
		vec3(float v) : x(v), y(v), z(v) {}
		vec3(float vx, float vy, float vz) : x(vx), y(vy), z(vz) {}
		vec3(const vec3& v) : x(v.x), y(v.y), z(v.z) {}

		void Normalize();
		static vec3 Cross(const vec3& a, const vec3& b);
		static float Dot(const vec3& a, const vec3& b);

		vec3& operator-=(const vec3& a) {
			x -= a.x;
			y -= a.y;
			z -= a.z;
			return *this;
		}

		vec3& operator+=(const vec3& a) {
			x += a.x;
			y += a.y;
			z += a.z;
			return *this;
		}

		float x, y, z;
	};

	inline vec3 operator-(const vec3& a, const vec3& b) {
		vec3 v;
		v.x = a.x - b.x;
		v.y = a.y - b.y;
		v.z = a.z - b.z;
		return v;
	}

	inline vec3 operator+(const vec3& a, const vec3& b) {
		vec3 v;
		v.x = a.x + b.x;
		v.y = a.y + b.y;
		v.z = a.z + b.z;
		return v;
	}

	inline vec3 operator*(const vec3& a, float b) {
		vec3 v;
		v.x = a.x * b;
		v.y = a.y * b;
		v.z = a.z * b;
		return v;
	}

}

#endif