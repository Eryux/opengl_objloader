#include "OBJLoader.h"

using namespace Esgi;

OBJLoader::OBJLoader() { }

int OBJLoader::findMaterial(std::string name, std::vector<material3D> mats) {
	int r = -1;

	for (int i = 0; i < mats.size(); i++) {
		if (mats.at(i).name == name) {
			r = i;
			break;
		}
	}

	return r;
}

object3D Esgi::OBJLoader::load(std::string filename, std::string path)
{
	object3D obj = object3D();

	// Parse OBJ File
	obj.meshes = this->_parseObject(path + filename);

	// Find and parse MTL file
	std::ifstream infile;
	infile.open(path + filename);
	
	if (!infile.is_open()) {
		std::cout << "OBJLoader - Can't open file " << filename << std::endl;
		return obj;
	}

	std::string line;
	while (std::getline(infile, line)) {
		if (line.size() == 0 || line == "")
			continue;

		if (line.find("mtllib") != std::string::npos) {

			std::vector<std::string> data = this->splitString(line, ' ');
			obj.materials = this->_parseMTL(path + data[1]);
		}
	}
	infile.close();


	return obj;
}

std::vector<material3D> OBJLoader::_parseMTL(std::string filename) {
	std::vector<material3D> mat;
	std::ifstream infile;

	infile.open(filename);

	if (!infile.is_open()) {
		std::cout << "OBJLoader - Can't open file " << filename << std::endl;
		return mat;
	}

	std::string line;
	material3D* m = nullptr;

	while (std::getline(infile, line)) {
		if (line.size() == 0 || line == "")
			continue;

		line = this->cleanSpaces(line);
		if (line.find("newmtl") != std::string::npos) {
			if (m != nullptr) {
				mat.push_back(*m);
				std::cout << m->name << " loaded " << m->texture << std::endl;
			}

			m = &(material3D());
			std::vector<std::string> data = this->splitString(line, ' ');
			m->name = data[1];
		}
		else if (m != nullptr) {
			std::vector<std::string> data = this->splitString(line, ' ');

			if (data[0].compare("Ns") == 0) {
				m->specular_level = std::stof(data[1]);
			}
			else if (data[0].compare("Ka") == 0) {
				m->ambient_color[0] = std::stof(data[1]);
				m->ambient_color[1] = std::stof(data[2]);
				m->ambient_color[2] = std::stof(data[3]);
			}
			else if (data[0].compare("Kd") == 0) {
				m->diffuse_color[0] = std::stof(data[1]);
				m->diffuse_color[1] = std::stof(data[2]);
				m->diffuse_color[2] = std::stof(data[3]);
			}
			else if (data[0].compare("Ks") == 0) {
				m->specular_color[0] = std::stof(data[1]);
				m->specular_color[1] = std::stof(data[2]);
				m->specular_color[2] = std::stof(data[3]);
			}
			else if (data[0].compare("Ke") == 0) {
				// Don't know what it does
			}
			else if (data[0].compare("Ni") == 0) {
				m->absorbance_level = std::stof(data[1]);
			}
			else if (data[0].compare("d") == 0) {
				m->opacity = std::stof(data[1]);
			}
			else if (data[0].compare("illum") == 0) {
				m->light = std::stoi(data[1]);
			}
			else if (data[0].compare("map_Kd") == 0) {
				m->texture = data[1];
			}
		}
	}

	if (m != nullptr) {
		mat.push_back(*m);
		std::cout << m->name << " loaded " << m->texture << std::endl;
	}

	infile.close();
	return mat;
}

std::vector<mesh3D> OBJLoader::_parseObject(std::string filename) {
	std::vector<mesh3D> obj;

	std::ifstream infile;
	infile.open(filename);

	if (!infile.is_open()) {
		std::cout << "OBJLoader - Can't open file " << filename << std::endl;
		return obj;
	}

	std::string line;
	mesh3D* o = nullptr;

	std::vector<float> coord_v;
	std::vector<float> coord_n;
	std::vector<float> coord_t;

	std::vector<float> coord_mesh;
	unsigned int vertex_count = 0;

	while (std::getline(infile, line)) {
		if (line.size() == 0 || line == "")
			continue;

		line = this->cleanSpaces(line);

		if (line.at(0) == 'o' || line.at(0) == 'g') {
			if (o != nullptr) {
				o->size_mesh = coord_mesh.size();
				o->mesh = new float[o->size_mesh];
				std::copy(coord_mesh.begin(), coord_mesh.end(), o->mesh);

				o->size_indexes = vertex_count;
				o->indexes = new uint16_t[vertex_count];
				for (int i = 0; i < vertex_count; i++) {
					o->indexes[i] = i;
				}
				std::cout << o->name << " loaded " << vertex_count << " v" << std::endl;
				vertex_count = 0;

				obj.push_back(*o);
			}

			coord_mesh.clear();

			o = &(mesh3D());
			std::vector<std::string> data_o = this->splitString(line, ' ');
			o->name = data_o[1];
		}
		else if (o != nullptr) {

			std::vector<std::string> data_o = this->splitString(line, ' ');
			if (data_o.size() == 0)
				continue;

			if (data_o[0].compare("v") == 0) {
				coord_v.push_back(std::stof(data_o[1]));
				coord_v.push_back(std::stof(data_o[2]));
				coord_v.push_back(std::stof(data_o[3]));
			}
			else if (data_o[0].compare("vn") == 0) {
				coord_n.push_back(std::stof(data_o[1]));
				coord_n.push_back(std::stof(data_o[2]));
				coord_n.push_back(std::stof(data_o[3]));
			}
			else if (data_o[0].compare("vt") == 0) {
				coord_t.push_back(std::stof(data_o[1]));
				coord_t.push_back(std::stof(data_o[2]));
			}
			else if (data_o[0].compare("usemtl") == 0) {
				o->material = data_o[1];
			}
			else if (data_o[0].compare("f") == 0) {
				if (data_o.size() != 4) {
					continue;
				}

				for (int i = 1; i < 4; i++) {
					std::vector<std::string> data_face = this->splitString(data_o[i], '/');

					if (data_face.size() == 3) {
						int begin = (std::stoi(data_face[0]) - 1) * 3;
						coord_mesh.push_back(coord_v.at(begin));
						coord_mesh.push_back(coord_v.at(begin + 1));
						coord_mesh.push_back(coord_v.at(begin + 2));

						begin = (std::stoi(data_face[2]) - 1) * 3;
						coord_mesh.push_back(coord_n.at(begin));
						coord_mesh.push_back(coord_n.at(begin + 1));
						coord_mesh.push_back(coord_n.at(begin + 2));

						begin = (std::stoi(data_face[1]) - 1) * 2;
						coord_mesh.push_back(coord_t.at(begin));
						coord_mesh.push_back(coord_t.at(begin + 1));
					}
					else if (data_face.size() == 2) {
						int begin = (std::stoi(data_face[0]) - 1) * 3;
						coord_mesh.push_back(coord_v.at(begin));
						coord_mesh.push_back(coord_v.at(begin + 1));
						coord_mesh.push_back(coord_v.at(begin + 2));

						begin = (std::stoi(data_face[1]) - 1) * 3;
						coord_mesh.push_back(coord_n.at(begin));
						coord_mesh.push_back(coord_n.at(begin + 1));
						coord_mesh.push_back(coord_n.at(begin + 2));

						coord_mesh.push_back(0.0f);
						coord_mesh.push_back(0.0f);
					}
					else {
						continue;
					}

					vertex_count += 1;
				}
			}
		}
	}

	if (o != nullptr) {
		o->size_mesh = coord_mesh.size();
		o->mesh = new float[o->size_mesh];
		std::copy(coord_mesh.begin(), coord_mesh.end(), o->mesh);

		o->size_indexes = vertex_count;
		o->indexes = new uint16_t[vertex_count];
		for (int i = 0; i < vertex_count; i++) {
			o->indexes[i] = i;
		}
		std::cout << o->name << " loaded " << vertex_count << " v" << std::endl;
		vertex_count = 0;

		obj.push_back(*o);
	}

	infile.close();
	return obj;
}

std::vector<std::string> Esgi::OBJLoader::splitString(std::string &in, const char &delimiter)
{
	std::string buff = "";
	std::vector<std::string> out;

	for (auto n : in) {
		if (n != delimiter) {
			buff += n;
		}
		else if (n == delimiter && buff != "") {
			out.push_back(buff);
			buff = "";
		}
	}

	if (buff != "") {
		out.push_back(buff);
	}

	return out;
}

std::string OBJLoader::cleanSpaces(std::string in) {
	unsigned int pos = -1;

	while ((pos = in.find("  ")) != std::string::npos) {
		in.erase(pos, 1);
	}

	return in;
}
