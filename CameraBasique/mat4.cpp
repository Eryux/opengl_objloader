#include "..\common\mat4.h"

using namespace Esgi;

void Mat4::Identity()
{
	memset(m, 0, sizeof(float) * 16);
	m[0] = 1.0f; m[5] = 1.0f; m[10] = 1.0f; m[15] = 1.0f;
}

void Mat4::MakeScale(float sx, float sy, float sz)
{
	memset(m, 0, sizeof(float) * 16);
	m[0] = sx; m[5] = sy; m[10] = sz; m[15] = 1.0f;
}

void Mat4::SetLocalScale(float sx, float sy, float sz)
{
	m[0] *= sx; m[5] *= sy; m[10] *= sz;
}

void Mat4::MakeRotation2D(float angleDegree)
{
	float theta = angleDegree * (M_PI / 180.0f);
	Identity();
	m[0] = cos(theta);
	m[1] = sin(theta);
	m[4] = -sin(theta);
	m[5] = cos(theta);
}

void Mat4::MakeTranslation(float tx, float ty, float tz)
{
	Identity();
	SetTranslate(tx, ty, tz);
}

void Mat4::SetTranslate(float tx, float ty, float tz)
{
	m[12] = tx; m[13] = ty; m[14] = tz;
}

void Mat4::Perspective(float fovy, float aspectRatio, float nearZ, float farZ)
{
	// Projection Matrix
	float fovy_rad = fovy * (M_PI / 180.f);
	float d = 1.0f / tan(fovy_rad * 0.5f);	// Distance focale

	float range = 1.0f / (nearZ - farZ);

	Identity();
	m[0] = d / aspectRatio;
	m[5] = d;
	m[10] = (nearZ + farZ)*range;
	m[11] = -1.0f;
	m[14] = (2.0f * nearZ * farZ)*range;
	m[15] = 0.0f;
}

void Mat4::MakeRotationY(float angleDegree)
{
	float theta = angleDegree * (M_PI / 180.0f);
	Identity();
	m[0] = cos(theta);
	m[2] = -sin(theta);
	m[8] = sin(theta);
	m[10] = cos(theta);
}

void Mat4::MakeRotationZ(float angleDegree)
{
	float theta = angleDegree * (M_PI / 180.0f);
	Identity();

	m[0] = cos(theta);
	m[1] = sin(theta);
	m[8] = -sin(theta);
	m[9] = cos(theta);
}

Mat4 Mat4::LookAt(const vec3& pos, const vec3& target, const vec3& up) {
	vec3 zaxis = target - pos; zaxis.Normalize(); // Direction
	vec3 nworld = up; nworld.Normalize();
	vec3 xaxis = vec3::Cross(nworld, zaxis); xaxis.Normalize(); // Right
	vec3 yaxis = vec3::Cross(zaxis, xaxis); // Up

	Mat4 r; r.Identity();
	r.m[0] = xaxis.x;
	r.m[4] = xaxis.y;
	r.m[8] = xaxis.z;

	r.m[1] = yaxis.x;
	r.m[5] = yaxis.y;
	r.m[9] = yaxis.z;

	r.m[2] = -zaxis.x;
	r.m[6] = -zaxis.y;
	r.m[10] = -zaxis.z;

	Mat4 translation;
	translation.MakeTranslation(-pos.x, -pos.y, -pos.z);

	return r * translation;
}