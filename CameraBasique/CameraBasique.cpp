#ifdef _MSC_VER
#pragma comment(lib, "opengl32.lib")
#include <windows.h>
#endif

#include <iostream>
#include <limits>

#include "DisplayGL.h"
#include "freeglut.h"

DisplayGL* programGL;

int g_mouseX = 0;
int g_mouseY = 0;

void update()
{
	glutPostRedisplay();
}

void animate()
{
	programGL->runGL();
}

void KeyboardEventManager(unsigned char key, int x, int y)
{
	if (programGL == nullptr) {
		return;
	}

	Esgi::Camera* camera = programGL->getCamera();

	if (key == 'a') {
		camera->m_fpsView = (!camera->m_fpsView);
		
		if (camera->m_fpsView)
			std::cout << "Switch to FPS view" << std::endl;
		else
			std::cout << "Switch to free fly view" << std::endl;
	}

	if (key == 'p') {
		programGL->togglePhong();
		std::cout << "Toggle phong" << std::endl;
	}

	if (key == 'w') {
		programGL->toggleWireframe();
		std::cout << "Toggle wireframe" << std::endl;
	}

	if (key == 't') {
		programGL->toggleTexture();
		std::cout << "Toggle texture" << std::endl;
	}

	if (key == '+') {
		programGL->getCamera()->m_speed += 5.0f;
	}
	if (key == '-') {
		programGL->getCamera()->m_speed -= 5.0f;
	}

	if (key == 'z')
		camera->MoveCamera(0, programGL->getDeltaTime());
	if (key == 's')
		camera->MoveCamera(1, programGL->getDeltaTime());
	if (key == 'q')
		camera->MoveCamera(2, programGL->getDeltaTime());
	if (key == 'd')
		camera->MoveCamera(3, programGL->getDeltaTime());
}

void KeyboardSpecialEventManager(int key, int x, int y)
{
	if (programGL == nullptr) {
		return;
	}

	Esgi::Camera* camera = programGL->getCamera();

	if (key == GLUT_KEY_UP)
		camera->MoveCamera(0, programGL->getDeltaTime());
	if (key == GLUT_KEY_DOWN)
		camera->MoveCamera(1, programGL->getDeltaTime());
	if (key == GLUT_KEY_LEFT)
		camera->MoveCamera(2, programGL->getDeltaTime());
	if (key == GLUT_KEY_RIGHT)
		camera->MoveCamera(3, programGL->getDeltaTime());
}

void MouseMoveEventManager(int x, int y)
{
	if (programGL == nullptr) {
		return;
	}

	Esgi::Camera* camera = programGL->getCamera();

	GLfloat xoffset = x - g_mouseX;
	GLfloat yoffset = y - g_mouseY;

	g_mouseX = x;
	g_mouseY = y;

	camera->TurnCamera(xoffset, yoffset);
}

int main(int argc, const char* argv[])
{
	if (argc < 3) {
		std::cout << "Two parameters is required" << std::endl;
		std::cout << "objloader.exe <obj_file> <path>" << std::endl;
		return 1;
	}

	glutInit(&argc, (char**)argv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);

	glutInitWindowPosition(200, 100);
	glutInitWindowSize(800, 800);
	glutCreateWindow("OBJ Loader");

#ifdef FREEGLUT
	glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);
#endif

	programGL = new DisplayGL(800, 800, true);

	programGL->loadOBJFile(std::string(argv[1]), std::string(argv[2]));
	//programGL->loadOBJFile("charizard-pokemon-go.obj", "../data/charizard/");
	//programGL->loadOBJFile("pikachu-pokemon-go.obj", "../data/pikachu/");
	//programGL->loadOBJFile("alduin-dragon.obj", "../data/alduin/");
	//programGL->loadOBJFile("german-panzer-ww2-ausf-b.obj", "../data/tank/");


	//programGL->toggleTexture();
	//programGL->loadOBJFile("cubetext.obj", "../data/");

	glutIdleFunc(update);
	glutDisplayFunc(animate);

	glutKeyboardFunc(KeyboardEventManager);
	glutSpecialFunc(KeyboardSpecialEventManager);
	glutPassiveMotionFunc(MouseMoveEventManager);
	glutMainLoop();

	programGL->terminateGL();
	delete programGL;

	return 1;
}