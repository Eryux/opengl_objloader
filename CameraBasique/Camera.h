#pragma once

#ifndef ESGI_CAMERA_H
#define ESGI_CAMERA_H

#include "vec3.h"
#include "../common/mat4.h"

#include "glew.h"
#ifdef _MSC_VER
#pragma comment(lib, "glew32.lib")
#endif

namespace Esgi
{
	class Camera 
	{
	public:
		vec3 m_position;
		vec3 m_front;
		vec3 m_up;
		vec3 m_right;
		vec3 m_worldup;

		GLfloat m_yaw;
		GLfloat m_pitch;
		GLfloat m_speed;
		GLfloat m_mouseSensitivity;

		GLboolean m_fpsView;

		Camera(vec3 position = vec3(0.f, 0.f, 0.f), vec3 up = vec3(0.f, 1.f, 0.f), GLfloat yaw = -90.f, GLfloat pitch = 0.f);
		Mat4 GetViewMatrix();

		void MoveCamera(int dir, GLfloat deltaTime);
		void TurnCamera(GLfloat x, GLfloat y, GLboolean lockPitch = true);

	private:
		void updateCameraVectors();
	};

}

#endif