#include "vec3.h"

using namespace Esgi;

void vec3::Normalize() {
	float l = sqrt((x*x) + (y*y) + (z*z));

	if (l != 0) {
		x = x / l;
		y = y / l;
		z = z / l;
	}
}

vec3 vec3::Cross(const vec3& a, const vec3& b) {
	float rx = a.y * b.z - a.z * b.y;
	float ry = a.z * b.x - a.x * b.z;
	float rz = a.x * b.y - a.y * b.x;

	return vec3(rx, ry, rz);
}

float vec3::Dot(const vec3& a, const vec3& b) {
	return (a.x*b.x + a.y * b.y + a.z * b.z);
}
