#pragma once

#ifndef DISPLAYGL_H
#define DISPLAYGL_H

#ifdef _MSC_VER
#pragma comment(lib, "opengl32.lib")
#include <windows.h>
#endif

#include <iostream>
#include <vector>

#include "glew.h"
#ifdef _MSC_VER
#pragma comment(lib, "glew32.lib")
#endif

#include "freeglut.h"

#include "../common/EsgiShader.h"
#include "../common/mat4.h"

#include "vec3.h"
#include "Camera.h"

#include "OBJLoader.h"

class DisplayGL {

	public:
		DisplayGL();
		DisplayGL(int w_width, int w_height);
		DisplayGL(int w_width, int w_height, bool debug);

		void runGL();
		float getDeltaTime();
		Esgi::Camera* getCamera();
		void toggleWireframe();
		void toggleTexture();
		void togglePhong();

		void loadOBJFile(std::string file, std::string path);
		void terminateGL();

	private:
		bool m_debug;
		bool m_wireframe = false;
		bool m_texture = true;
		bool m_phong = false;

		EsgiShader m_basicShader;
		EsgiShader m_lambertShader;
		EsgiShader m_phongShader;

		GLuint* m_vbo_array;
		GLuint* m_ibo_array;
		GLuint* m_tex_array;

		Esgi::object3D m_objectData;
		Esgi::Camera m_camera;

		float m_deltaTime = 0.f;
		float m_oldFrameTime = 0.f;

		int m_window_width = 800;
		int m_window_height = 600;

		bool _initialize();
		void _clearBuffer();
		int _findMaterial(std::string material);
};

#endif