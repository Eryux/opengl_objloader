#include "DisplayGL.h"

#define STB_IMAGE_IMPLEMENTATION
#include "../stb/stb_image.h"

using namespace std;

DisplayGL::DisplayGL() {
	m_debug = false;
	this->_initialize();
}

DisplayGL::DisplayGL(int w_width, int w_height) {
	m_debug = false;
	m_window_width = w_width;
	m_window_height = w_height;
	this->_initialize();
}

DisplayGL::DisplayGL(int w_width, int w_height, bool debug) {
	m_debug = debug;
	m_window_width = w_width;
	m_window_height = w_height;
	this->_initialize();
}

void DisplayGL::terminateGL() {
	if (m_vbo_array != nullptr) {
		for (int i = 0; i < m_objectData.meshes.size(); i++) {
			glDeleteBuffers(1, &m_vbo_array[i]);
		}
	}

	if (m_ibo_array != nullptr) {
		for (int i = 0; i < m_objectData.meshes.size(); i++) {
			glDeleteBuffers(1, &m_ibo_array[i]);
		}
	}
}

void DisplayGL::runGL() {
	// Refresh deltaTime
	int TimeSinceAppStartedInMS = glutGet(GLUT_ELAPSED_TIME);
	m_deltaTime = (TimeSinceAppStartedInMS - m_oldFrameTime) / 1000.0f;
	float TimeInSeconds = TimeSinceAppStartedInMS / 1000.0f;
	m_oldFrameTime = TimeSinceAppStartedInMS;

	// Refresh / Clear view
	glViewport(0, 0, m_window_width, m_window_height);
	glClearColor(0.05f, 0.05f, 0.05f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);

	// View
	Esgi::Mat4 worldMatrix = Esgi::Mat4::LookAt(m_camera.m_position, m_camera.m_position + m_camera.m_front, m_camera.m_up);
	
	Esgi::Mat4 projectionMatrix;
	projectionMatrix.Perspective(45.f, m_window_width / m_window_height, 0.1f, 1000.f);

	// Draw meshes
	if (&m_objectData != nullptr) {
		for (int i = 0; i < m_objectData.meshes.size(); i++) {

			uint32_t program;
			if (m_phong) 
				program = m_phongShader.GetProgram();
			else 
				program = m_lambertShader.GetProgram();

			glUseProgram(program);

			// Set light (lambert)
			auto LightAmbiantColor = glGetUniformLocation(program, "u_lightAmbientColor");
			glUniform3f(LightAmbiantColor, 0.9f, 0.9f, 0.9f);
			auto materialDiffuseColor = glGetUniformLocation(program, "u_materialDiffuseColor");
			glUniform3f(materialDiffuseColor, 1.f, 1.f, 1.f);
			auto LightDiffuseColor = glGetUniformLocation(program, "u_lightDiffuseColor");
			glUniform3f(LightDiffuseColor, 0.9f, 0.9f, 0.9f);
			auto materialAmbiantColor = glGetUniformLocation(program, "u_materialAmbientColor");
			glUniform3f(materialAmbiantColor, 1.f, 1.f, 1.f);

			if (m_phong) {
				auto materialSpecularColor = glGetUniformLocation(program, "u_materialSpecularColor");
				glUniform3f(materialSpecularColor, 1.f, 1.f, 1.f);
				auto materialSpecularLevel = glGetUniformLocation(program, "u_materialSpecularLevel");
				glUniform1f(materialSpecularLevel, 50.0f);
			}

			if (m_texture) {
				int m_index;
				if (m_objectData.meshes[i].material.size() != 0 && (m_index = this->_findMaterial(m_objectData.meshes[i].material)) != -1) {
					uint32_t texUnit = 0;
					glActiveTexture(GL_TEXTURE0 + texUnit);
					glBindTexture(GL_TEXTURE_2D, m_tex_array[m_objectData.materials[m_index].texture_index]);

					auto textureLocation = glGetUniformLocation(program, "u_Texture");
					glUniform1i(textureLocation, texUnit);

					if (m_objectData.materials[m_index].diffuse_color != nullptr)
						glUniform3fv(materialDiffuseColor, 3, m_objectData.materials[m_index].diffuse_color);
					if (m_objectData.materials[m_index].ambient_color != nullptr)
						glUniform3fv(materialAmbiantColor, 3, m_objectData.materials[m_index].ambient_color);
					
					if (m_phong) {
						auto materialSpecularColor = glGetUniformLocation(program, "u_materialSpecularColor");
						if (m_objectData.materials[m_index].specular_color != nullptr)
							glUniform3fv(materialSpecularColor, 3, m_objectData.materials[m_index].specular_color);

						auto materialSpecularLevel = glGetUniformLocation(program, "u_materialSpecularLevel");
						glUniform1f(materialSpecularLevel, m_objectData.materials[m_index].specular_level);
					}
				}
			}

			// Set object render position
			auto world_location = glGetUniformLocation(program, "u_WorldMatrix");
			glUniformMatrix4fv(world_location, 1, GL_FALSE, worldMatrix.m);
			auto projection_location = glGetUniformLocation(program, "u_ProjectionMatrix");
			glUniformMatrix4fv(projection_location, 1, GL_FALSE, projectionMatrix.m);

			// Set time (required for animation only)
			auto time_location = glGetUniformLocation(program, "u_Time");
			glUniform1f(time_location, TimeInSeconds);

			// Set light
			auto lightDir_location = glGetUniformLocation(program, "u_PositionOrDirection");
			float lightVector[4] = { 0.0, 10.0, 0.0, 0.0 };
			glUniform4fv(lightDir_location, 1, lightVector);

			// Set coords
			auto position_location = glGetAttribLocation(program, "a_Position");
			auto texcoords_location = glGetAttribLocation(program, "a_TexCoords");
			auto normal_location = glGetAttribLocation(program, "a_Normal");

			// Vertex position
			glBindBuffer(GL_ARRAY_BUFFER, m_vbo_array[i]);
			glVertexAttribPointer(position_location, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), reinterpret_cast<const void *>(0 * sizeof(float)));
			glEnableVertexAttribArray(position_location);

			// Normal position
			glVertexAttribPointer(normal_location, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), reinterpret_cast<const void *>(3 * sizeof(float)));
			glEnableVertexAttribArray(normal_location);

			// UV position
			glVertexAttribPointer(texcoords_location, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), reinterpret_cast<const void *>(6 * sizeof(float)));
			glEnableVertexAttribArray(texcoords_location);

			// Indexes
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo_array[i]);
			glDrawElements(GL_TRIANGLES, m_objectData.meshes[i].size_indexes, GL_UNSIGNED_SHORT, nullptr);

			glDisableVertexAttribArray(position_location);
		}
	}

	glUseProgram(0);
	glutSwapBuffers();
}

float DisplayGL::getDeltaTime() {
	return m_deltaTime;
}

Esgi::Camera* DisplayGL::getCamera() {
	return &(m_camera);
}

void DisplayGL::loadOBJFile(std::string file, std::string path) {
	Esgi::OBJLoader loader = Esgi::OBJLoader();
	m_objectData = loader.load(file, path);

	// Creation des buffers
	if (&m_objectData == nullptr) {
		return;
	}

	this->_clearBuffer();
	m_vbo_array = new GLuint[m_objectData.meshes.size()];
	m_ibo_array = new GLuint[m_objectData.meshes.size()];

	for (int i = 0; i < m_objectData.meshes.size(); i++) {
		glGenBuffers(1, &m_vbo_array[i]);
		glBindBuffer(GL_ARRAY_BUFFER, m_vbo_array[i]);
		glBufferData(GL_ARRAY_BUFFER, m_objectData.meshes[i].size_mesh * sizeof(float), m_objectData.meshes[i].mesh, GL_STATIC_DRAW);
	
		glGenBuffers(1, &m_ibo_array[i]);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_ibo_array[i]);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_objectData.meshes[i].size_indexes * sizeof(GLushort), m_objectData.meshes[i].indexes, GL_STATIC_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

	int c = 0;
	for (int i = 0; i < m_objectData.materials.size(); i++) {
		if (m_objectData.materials[i].texture.size() == 0) {
			continue;
		}

		c++;
	}

	int j = 0;
	m_tex_array = new GLuint[c];
	for (int i = 0; i < m_objectData.materials.size(); i++) {
		if (m_objectData.materials[i].texture.size() == 0) {
			continue;
		}

		int w, h, c;
		std::string filepath = path + m_objectData.materials[j].texture;
		uint8_t* bitmapRGBA = stbi_load(filepath.c_str(), &w, &h, &c, STBI_rgb_alpha);

		glGenTextures(1, &m_tex_array[j]);
		glBindTexture(GL_TEXTURE_2D, m_tex_array[j]);

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, bitmapRGBA);
		glGenerateMipmap(GL_TEXTURE_2D);
		stbi_image_free(bitmapRGBA);
		glBindTexture(GL_TEXTURE_2D, 0);

		m_objectData.materials[i].texture_index = j;
		j++;
	}
}

bool DisplayGL::_initialize() {
	glewInit();

	// Chargement des shaders
	m_lambertShader.LoadVertexShader("shaders/lambert.vs");
	m_lambertShader.LoadFragmentShader("shaders/lambert.fs");
	m_lambertShader.CreateProgram();

	m_phongShader.LoadVertexShader("shaders/phong.vs");
	m_phongShader.LoadFragmentShader("shaders/phong.fs");
	m_phongShader.CreateProgram();

	m_basicShader.LoadVertexShader("shaders/basic.vs");
	m_basicShader.LoadFragmentShader("shaders/basic.fs");
	m_basicShader.CreateProgram();

	// Chargement et reglage camera
	m_camera = Esgi::Camera(Esgi::vec3(0.f, 5.f, -20.f));
	m_camera.m_speed = 20.f;
	m_camera.m_fpsView = true;

	return true;
}

void DisplayGL::toggleWireframe() {
	if (m_wireframe) {
		glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}
	else {
		glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	}

	m_wireframe = !m_wireframe;
}

void DisplayGL::toggleTexture() {
	m_texture = !m_texture;
}

void DisplayGL::togglePhong() {
	m_phong = !m_phong;
}

void DisplayGL::_clearBuffer() {
	delete[] m_vbo_array;
	delete[] m_ibo_array;
}

int DisplayGL::_findMaterial(std::string material) {
	for (int i = 0; i < m_objectData.materials.size(); i++) {
		if (m_objectData.materials[i].name == material) {
			return i;
		}
	}

	return -1;
}