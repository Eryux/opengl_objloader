#pragma once
#ifndef OBJLOADER_H
#define OBJLOADER_H

#include <fstream>
#include <string>
#include <vector>
#include <iostream>
#include <sstream>
#include <algorithm>

namespace Esgi 
{

	struct mesh3D {
		std::string name;
		std::string material;

		float* mesh;
		uint16_t* indexes;

		unsigned int size_mesh;
		unsigned int size_indexes;
	};

	struct material3D {
		std::string name;

		float ambient_color[3];
		float diffuse_color[3];
		float specular_color[3];
		float transmission[3];

		float absorbance_level = 0.0f;
		float specular_level = 0.5f;

		float opacity = 1.0f;

		unsigned int light;

		std::string texture;
		unsigned int texture_index;
	};

	struct object3D {
		std::vector<mesh3D> meshes;
		std::vector<material3D> materials;
	};

	class OBJLoader
	{

	public:
		OBJLoader();
		object3D load(std::string filename, std::string path);
		int findMaterial(std::string name, std::vector<material3D> mats);

	private:
		std::vector<mesh3D> _parseObject(std::string filename);
		std::vector<material3D> _parseMTL(std::string filename);
		std::vector<std::string> splitString(std::string &in, const char &del);
		std::string cleanSpaces(std::string in);
	};

}


#endif // !OBJLOADER_H

