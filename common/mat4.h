#pragma once

#ifndef ESGI_MAT4_H
#define ESGI_MAT4_H

#include <cstring>
#define _USE_MATH_DEFINES
#include <cmath>

#include "vec3.h"

namespace Esgi
{

	struct Mat4
	{
		float m[16];
		void Identity();
		void MakeScale(float sx, float sy, float sz);
		void SetLocalScale(float sx, float sy, float sz);
		void MakeRotation2D(float angleDegree);
		void MakeTranslation(float tx, float ty, float tz);
		void SetTranslate(float tx, float ty, float tz);

		void Perspective(float fovy, float aspect, float nearZ, float farZ);
		void MakeRotationY(float angleDegree);
		void MakeRotationZ(float angleDegree);

		static Mat4 LookAt(const vec3& src, const vec3& target, const vec3& dir);
	};

	inline Mat4 operator*(const Esgi::Mat4& A, Esgi::Mat4& B)
	{
		// Mij = Sum(k) Aik * Bkj
		Mat4 M;

		memset(M.m, 0, sizeof(float) * 16);
		for (int j = 0; j < 4; j++) {
			for (int i = 0; i < 4; i++) {
				for (int k = 0; k < 4; k++) {
					M.m[i + j * 4] += A.m[i + k * 4] * B.m[k + j * 4];
				}
			}
		}

		return M;
	}

}; // Esgi

#endif // ESGI_MAT4_H
