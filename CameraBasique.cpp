
#ifdef _MSC_VER
#pragma comment(lib, "opengl32.lib")
#include <windows.h>
#endif

#include "glew.h"
#ifdef _MSC_VER
#pragma comment(lib, "glew32.lib")
#endif

#include "freeglut.h"

#include "../common/EsgiShader.h"
#include "../common/mat4.h"

#include "vec3.h"
#include "Camera.h"

#define STB_IMAGE_IMPLEMENTATION
#include "../stb/stb_image.h"

// format des vertices : X, Y, Z, ?, ?, ?, ?, ? = 8 floats
#include "../data/DragonData.h"

#include <iostream>
#include <vector>

#include "OBJLoader.h"

#if _MSC_VER
uint32_t dragonVertexCount = _countof(DragonVertices);
uint32_t dragonIndexCount = _countof(DragonIndices);
#endif

EsgiShader g_BasicShader;

int width = 800;
int height = 600;

GLuint VBO;	// identifiant du Vertex Buffer Object
GLuint IBO;	// identifiant du Index Buffer Object
GLuint TexObj; // identifiant du Texture Object

GLuint VBO_2;	// identifiant du Vertex Buffer Object
GLuint IBO_2;	// identifiant du Index Buffer Object
GLuint TexObj_2; // identifiant du Texture Object

// Camera
Esgi::Camera m_camera;

float m_deltaTime = 0.f;
float m_oldFrameTime = 0.f;

int lastMouseX = 0;
int lastMouseY = 0;

// OBJ
Esgi::object3D m_mesh;

bool Initialize()
{
	glewInit();

	g_BasicShader.LoadVertexShader("basicLight.vs");
	g_BasicShader.LoadFragmentShader("basicLight.fs");
	g_BasicShader.CreateProgram();

	//glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	glGenTextures(1, &TexObj);
	glBindTexture(GL_TEXTURE_2D, TexObj);
	int w, h, c; // largeur, hauteur, #de composantes du fichier
	uint8_t* bitmapRGBA = stbi_load("../data/dragon.png",
		&w, &h, &c, STBI_rgb_alpha);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);//GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, w, h, 0	// destination
		, GL_RGBA, GL_UNSIGNED_BYTE, bitmapRGBA); // source
	glGenerateMipmap(GL_TEXTURE_2D);
	stbi_image_free(bitmapRGBA);
	glBindTexture(GL_TEXTURE_2D, 0);

	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, dragonVertexCount * sizeof(float)
		, DragonVertices, GL_STATIC_DRAW);
	glGenBuffers(1, &IBO);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, dragonIndexCount * sizeof(GLushort)
		, DragonIndices, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// Chargement de l'obj
	Esgi::OBJLoader loader = Esgi::OBJLoader();
	m_mesh = loader.load("cube.obj", "../data/");

	glGenBuffers(1, &VBO_2);
	glBindBuffer(GL_ARRAY_BUFFER, VBO_2);
	glBufferData(GL_ARRAY_BUFFER, m_mesh.meshes[0].size_mesh * sizeof(float), m_mesh.meshes[0].mesh, GL_STATIC_DRAW);
	
	glGenBuffers(1, &IBO_2);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO_2);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_mesh.meshes[0].size_indexes * sizeof(GLushort), m_mesh.meshes[0].indexes, GL_STATIC_DRAW);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);


	// Reglage de la camera ---------------------
	m_camera = Esgi::Camera(Esgi::vec3(0.f, 5.f, -20.f));
	m_camera.m_speed = 10.f;
	m_camera.m_fpsView = true;

	return true;
}

void Terminate()
{
	glDeleteTextures(1, &TexObj);
	glDeleteBuffers(1, &IBO);
	glDeleteBuffers(1, &VBO);

	g_BasicShader.DestroyProgram();
}

void update()
{
	glutPostRedisplay();
}

void animate()
{
	int TimeSinceAppStartedInMS = glutGet(GLUT_ELAPSED_TIME);
	m_deltaTime = (TimeSinceAppStartedInMS - m_oldFrameTime) / 1000.0f;

	float TimeInSeconds = TimeSinceAppStartedInMS / 1000.0f;
	m_oldFrameTime = TimeSinceAppStartedInMS;

	glViewport(0, 0, width, height);
	glClearColor(0.9f, 0.8f, 0.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glEnable(GL_DEPTH_TEST);

	auto program = g_BasicShader.GetProgram();
	glUseProgram(program);

	uint32_t texUnit = 0;
	glActiveTexture(GL_TEXTURE0 + texUnit);
	glBindTexture(GL_TEXTURE_2D, TexObj);
	auto textureLocation = glGetUniformLocation(program, "u_Texture");
	glUniform1i(textureLocation, texUnit);

	// UNIFORMS
	// View matrix ------------------------------
	Esgi::Mat4 worldMatrix = Esgi::Mat4::LookAt(m_camera.m_position, m_camera.m_position + m_camera.m_front, m_camera.m_up);

	auto world_location = glGetUniformLocation(program, "u_WorldMatrix");
	glUniformMatrix4fv(world_location, 1, GL_FALSE, worldMatrix.m);

	// ------------------------------------------

	Esgi::Mat4 projectionMatrix;

	float w = glutGet(GLUT_WINDOW_WIDTH), h = glutGet(GLUT_WINDOW_HEIGHT);
	float nearZ = 0.1f;
	float farZ = 1000.0f;
	float aspectRatio = w / h;	// facteur d'aspect
	float fovy = 45.0f;			// degre d'ouverture du field of view
	projectionMatrix.Perspective(fovy, aspectRatio, nearZ, farZ);

	auto projection_location = glGetUniformLocation(program, "u_ProjectionMatrix");
	glUniformMatrix4fv(projection_location, 1, GL_FALSE, projectionMatrix.m);

	auto time_location = glGetUniformLocation(program, "u_Time");
	glUniform1f(time_location, TimeInSeconds);

	auto lightDir_location = glGetUniformLocation(program
		, "u_PositionOrDirection");
	float lightVector[4] = { 0.0, 1.0, 0.0, 0.0 };
	glUniform4fv(lightDir_location, 1, lightVector);

	// ATTRIBUTES
	auto position_location = glGetAttribLocation(program, "a_Position");
	auto texcoords_location = glGetAttribLocation(program, "a_TexCoords");
	auto normal_location = glGetAttribLocation(program, "a_Normal");

	// Le fait de specifier la ligne suivante va modifier le fonctionnement interne de glVertexAttribPointer
	// lorsque GL_ARRAY_BUFFER != 0 cela indique que les donnees sont stockees sur le GPU
	//glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO_2);

	// le VBO contient des vertex au format suivant:
	// X,Y,Z,NX,NY,NZ,U,V
	/*struct Vertex
	{
		float x, y, z;		// offset = 0
		float nx, ny, nz;	// offset = 3
		float u, v;			// offset = 6
	};

	// 1er cas : on a l'adresse du tableau original
	Vertex* v = (Vertex*)DragonVertices;
	float *adresse = &v->u;
	size_t adresseRelative = (adresse - DragonVertices) * sizeof(float);*/
	// 2nd cas: on ne dispose de l'adresse du tableau DragonVertices
	//size_t rel = offsetof(Vertex, u);

	glVertexAttribPointer(position_location, 3, GL_FLOAT, GL_FALSE
		, 8 * sizeof(float)
		, reinterpret_cast<const void *>(0 * sizeof(float)));
	glEnableVertexAttribArray(position_location);
	// on interprete les 3 valeurs inconnues comme du RGB
	glVertexAttribPointer(normal_location, 3, GL_FLOAT, GL_FALSE
		, 8 * sizeof(float)
		, reinterpret_cast<const void *>(3 * sizeof(float)));
	glEnableVertexAttribArray(normal_location);
	glVertexAttribPointer(texcoords_location, 2, GL_FLOAT, GL_FALSE
		, 8 * sizeof(float)
		, reinterpret_cast<const void *>(6 * sizeof(float)));
	glEnableVertexAttribArray(texcoords_location);


	//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO);
	//glDrawElements(GL_TRIANGLES, dragonIndexCount, GL_UNSIGNED_SHORT, nullptr);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, IBO_2);
	glDrawElements(GL_TRIANGLES, m_mesh.meshes[0].size_indexes, GL_UNSIGNED_SHORT, nullptr);

	glDisableVertexAttribArray(position_location);
	glUseProgram(0);
	glutSwapBuffers();
}

void KeyboardEventManager(unsigned char key, int x, int y)
{
	if (key == 'a') {
		m_camera.m_fpsView = (!m_camera.m_fpsView);
		
		if (m_camera.m_fpsView)
			std::cout << "Switch to FPS view" << std::endl;
		else
			std::cout << "Switch to free fly view" << std::endl;
	}

	if (key == 'z')
		m_camera.MoveCamera(0, m_deltaTime);
	if (key == 's')
		m_camera.MoveCamera(1, m_deltaTime);
	if (key == 'q')
		m_camera.MoveCamera(2, m_deltaTime);
	if (key == 'd')
		m_camera.MoveCamera(3, m_deltaTime);
}

void KeyboardSpecialEventManager(int key, int x, int y)
{
	if (key == GLUT_KEY_UP)
		m_camera.MoveCamera(0, m_deltaTime);
	if (key == GLUT_KEY_DOWN)
		m_camera.MoveCamera(1, m_deltaTime);
	if (key == GLUT_KEY_LEFT)
		m_camera.MoveCamera(2, m_deltaTime);
	if (key == GLUT_KEY_RIGHT)
		m_camera.MoveCamera(3, m_deltaTime);
}

void MouseMoveEventManager(int x, int y)
{
	GLfloat xoffset = x - lastMouseX;
	GLfloat yoffset = y - lastMouseY;

	lastMouseX = x;
	lastMouseY = y;

	m_camera.TurnCamera(xoffset, yoffset);
}

int main(int argc, const char* argv[])
{
	// passe les parametres de la ligne de commande a glut
	glutInit(&argc, (char**)argv);
	// defini deux color buffers (un visible, un cache) RGBA
	// GLUT_DEPTH alloue egalement une zone memoire pour le depth buffer
	glutInitDisplayMode(GLUT_RGBA | GLUT_DEPTH | GLUT_DOUBLE);
	// positionne et dimensionne la fenetre
	glutInitWindowPosition(200, 100);
	glutInitWindowSize(width, height);
	// creation de la fenetre ainsi que du contexte de rendu
	glutCreateWindow("TP OpenGL Camera");

#ifdef FREEGLUT
	glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE, GLUT_ACTION_GLUTMAINLOOP_RETURNS);
#endif

	if (Initialize() == false)
		return -1;

	glutIdleFunc(update);
	glutDisplayFunc(animate);

	glutKeyboardFunc(KeyboardEventManager);
	glutSpecialFunc(KeyboardSpecialEventManager);
	glutPassiveMotionFunc(MouseMoveEventManager);
	glutMainLoop();

	Terminate();

	return 1;
}